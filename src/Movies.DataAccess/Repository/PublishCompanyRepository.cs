﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;

namespace Movies.DataAccess.Repository
{
    public class PublishCompanyRepository : GenericRepository<PublishCompany>, IPublishCompanyRepository
    {
        private readonly ApplicationDbContext _db;
        public PublishCompanyRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
