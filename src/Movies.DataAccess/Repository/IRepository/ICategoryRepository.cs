﻿using Movies.DomainClass.Models;

namespace Movies.DataAccess.Repository.IRepository
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
    }
}
