﻿using System;

namespace Movies.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork : IDisposable
    {
        IMovieRepository Movie { get; }
        ICategoryRepository Category { get; }
        IPublishCompanyRepository PublishCompany { get; }
        void Save();
    }
}
