﻿using Movies.DomainClass.Models;

namespace Movies.DataAccess.Repository.IRepository
{
    public interface IMovieRepository :  IGenericRepository<Movie>
    {
    }
}
