﻿using Movies.DomainClass.Models;

namespace Movies.DataAccess.Repository.IRepository
{
    public interface IPublishCompanyRepository : IGenericRepository<PublishCompany>
    {
    }
}
