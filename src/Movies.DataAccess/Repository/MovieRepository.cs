﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;

namespace Movies.DataAccess.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        private readonly ApplicationDbContext _db;
        public MovieRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
    }
}
