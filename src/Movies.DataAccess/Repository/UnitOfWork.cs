﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;
using System;
using static System.Net.Mime.MediaTypeNames;

namespace Movies.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private ApplicationDbContext _context;
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Movie = new MovieRepository(_context);
            Category = new CategoryRepository(_context);
            PublishCompany = new PublishCompanyRepository(_context);
        }
        public IMovieRepository Movie { get; private set; }

        public ICategoryRepository Category { get; private set; }

        public IPublishCompanyRepository PublishCompany { get; private set; }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
