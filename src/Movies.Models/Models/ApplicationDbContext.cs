using System.Configuration;
using System.Data.Entity;

namespace Movies.DomainClass.Models
{
    public partial class ApplicationDbContext : DbContext
    {
        private static readonly string conn = ConfigurationManager.ConnectionStrings["MovieDb"].ConnectionString;
        public ApplicationDbContext()
            : base(conn)
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Movie> Movies { get; set; }
        public virtual DbSet<PublishCompany> PublishCompanies { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasMany(e => e.Movies)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PublishCompany>()
                .HasMany(e => e.Movies)
                .WithRequired(e => e.PublishCompany)
                .WillCascadeOnDelete(false);
        }
    }
}
