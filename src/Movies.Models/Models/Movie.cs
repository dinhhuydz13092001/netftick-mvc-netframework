namespace Movies.DomainClass.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Movie")]
    public partial class Movie
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Title { get; set; }

        public double Hitting { get; set; }

        public int CategoryId { get; set; }

        public DateTime CreateAt { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public int PublishCompanyId { get; set; }

        [StringLength(500)]
        public string Image { get; set; }

        public virtual Category Category { get; set; }

        public virtual PublishCompany PublishCompany { get; set; }
    }
}
