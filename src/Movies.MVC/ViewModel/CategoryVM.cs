﻿namespace Movies.MVC.ViewModel
{
    public class CategoryVM
    {
        public string Name { get; set; }    
        public string Description { get; set; }
    }
}