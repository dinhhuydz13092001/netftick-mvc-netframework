﻿namespace Movies.MVC.ViewModel
{
    public class DetailMovieVM
    {
        public string Title { get; set; }
        public double Hitting { get; set; }
        public string CreateAt { get; set; }
        public string ImageLink { get; set; }
        public string PublishCompanyName { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
    }
}