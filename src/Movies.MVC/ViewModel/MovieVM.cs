﻿namespace Movies.MVC.ViewModel
{
    public class MovieVM
    {
        public int id { get; set; }
        public string Title { get; set; }
        public double Hitting { get; set; }
        public string Release {  get; set; }

    }
}