﻿using Movies.DomainClass.Models;
using Movies.MVC.ViewModel;
using System.Collections.Generic;

namespace Movies.MVC.Helper
{
    public static class ViewModelHelper
    {
        public static MovieVM ToVM(this Movie movie) { 
            return new MovieVM() { 
                id = movie.Id,
                Title = movie.Title,
                Hitting = movie.Hitting,
                Release = movie.CreateAt.ToString("dd/MM/yyyy"),
            }; 
        }

        public static IEnumerable<MovieVM> ToListVM(this IEnumerable<Movie> movies)
        {
            List<MovieVM> movieVMs = new List<MovieVM>();
            foreach (Movie movie in movies)
            {
                movieVMs.Add(ToVM(movie));
            }
            return movieVMs;
        }

        public static DetailMovieVM ToDetailVM(this Movie movie)
        {
            return new DetailMovieVM()
            {
                Title = movie.Title,
                CategoryName = movie.Category.Name,
                CreateAt = movie.CreateAt.ToString("yyyy/MM/dd"),
                ImageLink = movie.Image,
                Description = movie.Description,
                Hitting = movie.Hitting,
                PublishCompanyName = movie.PublishCompany.Name
            };         
        }

        public static UpdateMovieVM ToUpdateVM(this Movie movie)
        {
            return new UpdateMovieVM()
            {
                id = movie.Id,
                Title = movie.Title,
                CategoryName = movie.Category.Name,
                CreateAt = movie.CreateAt.ToString("yyyy/MM/dd"),
                ImageLink = movie.Image,
                Description = movie.Description,
                Hitting = movie.Hitting,
                PublishCompanyName = movie.PublishCompany.Name
            };
        }


        private static CategoryVM ToVM(this Category category)
        {
            return new CategoryVM() { 
                Name = category.Name,
                Description = category.Description,
            };
        }

        public static List<CategoryVM> ToListVM(this IEnumerable<Category> categories)
        {
            List<CategoryVM> listVM = new List<CategoryVM>();
            foreach(Category category in categories)
            {
                listVM.Add(ToVM(category));
            }

            return listVM;
        }
    }
}