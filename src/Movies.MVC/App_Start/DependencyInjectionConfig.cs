﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DataAccess.Repository;
using Autofac.Integration.Mvc;
using Autofac;
using Movies.DomainClass.Models;
using System.Reflection;
using System.Web.Mvc;

namespace Movies.MVC.App_Start
{
    public class DependencyInjectionConfig
    {
        public static void RegisterService()
        {
            var containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterType<ApplicationDbContext>().InstancePerLifetimeScope();
            containerBuilder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            containerBuilder.RegisterControllers(Assembly.GetExecutingAssembly());

            var container = containerBuilder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}