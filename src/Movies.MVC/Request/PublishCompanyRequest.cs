﻿namespace Movies.MVC.Request
{
    public class PublishCompanyRequest
    {
        public string Name { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
    }
}