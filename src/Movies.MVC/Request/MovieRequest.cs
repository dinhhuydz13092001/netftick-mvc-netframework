﻿namespace Movies.MVC.Request
{
    public class MovieRequest
    {
        public string FilmName { get; set; }
        public string Hitting { get; set; }
        public string Category { get; set; }
        public string Company { get; set; }
        public string PublishDay { get; set; }
        public string ImageLink { get; set; }
        public string Description { get; set; }
    }
}