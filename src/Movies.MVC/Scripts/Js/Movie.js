﻿$('#btn-create').on('click', function (event) {

    var filmName = $('#filmName').val();
    var hitting = $('#hitting').val();
    var category = $('#category option:selected').val();
    var company = $('#company option:selected').val();
    var publishDay = $('#publishDay').val();
    var imageLink = $('#imageLink').val();
    var description = $('#description').val();

    var data = {
        FilmName: filmName,
        Hitting: hitting,
        Category: category,
        Company: company,
        PublishDay: publishDay,
        ImageLink: imageLink,
        Description: description
    }; 


    $.ajax({
        type: 'POST',
        url: '/Movie/Create',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                Swal.fire({
                    title: "Success",
                    text: "Completed create movie!",
                    icon: "success",
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Go to list"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/Movie/List'
                    }
                });
            }
            else {
                let errorField = '#' + response.errorField + '-error';
                let message = response.message;
                $(errorField).text(message);
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
});

$('#btn-update').on('click', function (event) {
    event.preventDefault();

    var id = $('#movieId').text();
    var filmName = $('#filmName').val();
    var hitting = $('#hitting').val();
    var category = $('#category option:selected').val();
    var company = $('#company option:selected').val();
    var publishDay = $('#publishDay').val();
    var imageLink = $('#imageLink').val();
    var description = $('#description').val();

    var request = {
        FilmName: filmName,
        Hitting: hitting,
        Category: category,
        Company: company,
        PublishDay: publishDay,
        ImageLink: imageLink,
        Description: description
    };

    var data = {
        request: request,
        id: id
    }


    $.ajax({
        type: 'POST',
        url: '/Movie/Update',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                Swal.fire({
                    title: "Success",
                    text: "Completed Update movie!",
                    icon: "success",
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Go to list"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/Movie/List'
                    }
                });
            }
            else {
                let errorField = '#' + response.errorField + '-error';
                let message = response.message;
                $(errorField).text(message);
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
});

function InItPicker() {
    $('#publishDay').datepicker();
}

InItPicker();

function RemoveErrorMessage() {
    $('#hitting').on('click', function () {
        $('#hitting-error').text('');
    });

    $('#filmName').on('click', function () {
        $('#filmName-error').text('');
    });

    $('#imageLink').on('click', function () {
        $('#imageLink-error').text('');
    });

    $('#description').on('click', function () {
        $('#description-error').text('');
    });

    $('#Category').on('click', function () {
        $('#Category-error').text('');
    });

    $('#company').on('click', function () {
        $('#company-error').text('');
    });

    $('#publishDay').on('click', function () {
        $('#publishDay-error').text('');
    });
}

RemoveErrorMessage();

function SearchMovie() {
    $('#search-movie').on('click', function () {
        let searchValue = $('#search-value').val();
        window.location.href = '/movie/list?fillter=' + searchValue;
    });
}

SearchMovie();

function Paginated() {
    $('.btn-paginated').on('click', function () {
        var value = $(this).text();
        var fillter = $('#is-fillter').text();
        var sort = $('#is-sort').text();

        var url = '/movie/list?page=' + value;

        if (sort !== '') {
            url += '&sort=' + sort;
        }

        if (fillter !== '') {
            url += '&fillter=' + fillter;
        }

        window.location.href = url;
    });
}

Paginated();

function PreviousPage(currentPage) {
    if (currentPage > 1) {
        const fillter = $("#is-fillter").text();
        const sort = $("#is-sort").text();
        let url = "/movie/list?page=" + (currentPage - 1);
        if (sort != '') {
            url += "&sort=" + sort;
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    }
}

function NextPage(currentPage, totalPage) {
    if (currentPage < totalPage) {
        const fillter = $("#is-fillter").text();
        const sort = $("#is-sort").text();
        let url = "/movie/list?page=" + (currentPage + 1);
        if (sort != '') {
            url += "&sort=" + sort;
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    }
}

function ChangeColorPagenatedButton() {
    const pageIndex = $("#spn-currenPage").text();
    $('.btn-paginated').each(function () {
        if ($(this).text() == pageIndex) {
            $(this).removeClass('btn-outline-primary')
                .addClass('btn-primary');
        }
    });
}

ChangeColorPagenatedButton();

function RedirectUpdate(id) {
    window.location.href = '/Movie/Update?id=' + id;
}

function RedirectCreate() {
    window.location.href = '/Movie/Create';
}

function Delete(id) {
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/Movie/Delete/?id=' + id,
                type: 'GET',
                success: function (response) {
                    if (response.success) {
                        Swal.fire({
                            title: "Success",
                            text: "Completed delete movie!",
                            icon: "success",
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Go to list"
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = '/Movie/List'
                            }
                        });
                    }
                    else {
                        Swal.fire({
                            title: "Fail!",
                            text: response.message,
                            icon: "error"
                        });
                    }
                },
                error: function () {
                    console.error('Failed to delete data');
                }
            });
        }
    });
}
