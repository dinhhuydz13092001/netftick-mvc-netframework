﻿function SearchCategory() {
    $('#search-category').on('click', function () {
        let searchValue = $('#search-value').val();
        window.location.href = '/category/list?fillter=' + searchValue;
    });
}

SearchCategory();

$('#btn-create').on('click', function (event) {

    var categoryName = $('#categoryName').val();
    var description = $('#description').val();

    var data = {
        Name: categoryName,
        Description: description
    };


    $.ajax({
        type: 'POST',
        url: '/Category/Create',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                Swal.fire({
                    title: "Success",
                    text: "Completed create film gener!",
                    icon: "success",
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Go to list"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/Category/List'
                    }
                });
            }
            else {
                let errorField = '#' + response.errorField + '-error';
                let message = response.message;
                $(errorField).text(message);
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
});

$('#btn-update').on('click', function (event) {

    var id = $('#categoryId').text();
    var categoryName = $('#categoryName').val();
    var description = $('#description').val();

    var data = {
        Id: id,
        Name: categoryName,
        Description: description
    };


    $.ajax({
        type: 'POST',
        url: '/Category/Update',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                Swal.fire({
                    title: "Success",
                    text: "Completed Update Category!",
                    icon: "success",
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Go to list"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/Category/List'
                    }
                });
            }
            else {
                let errorField = '#' + response.errorField + '-error';
                let message = response.message;
                $(errorField).text(message);
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
});

function Delete(id) {
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/Category/Delete/?id=' + id,
                type: 'GET',
                success: function (response) {
                    if (response.success) {
                        Swal.fire({
                            title: "Success",
                            text: "Completed delete category!",
                            icon: "success",
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Go to list"
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = '/Category/List'
                            }
                        });
                    }
                    else {
                        Swal.fire({
                            title: "Fail!",
                            text: response.message,
                            icon: "error"
                        });
                    }
                },
                error: function () {
                    console.error('Failed to delete data');
                }
            });
        }
    });
}

function RedirectUpdate(id) {
    window.location.href = '/Category/Update?id=' + id;
}

function RedirectCreate() {
    window.location.href = '/Category/Create';
}

function RemoveErrorMessage() {
    $('#categoryName').on('click', function () {
        $('#categoryName-error').text('');
    });

    $('#description').on('click', function () {
        $('#description-error').text('');
    });
}

RemoveErrorMessage();
