﻿document.addEventListener('DOMContentLoaded', function () {
    // 설치
    const container = document.getElementById('realgrid');
    const provider = new RealGrid.LocalDataProvider(false);
    const gridView = new RealGrid.GridView(container);
    gridView.setDataSource(provider);

    // 필드 생성
    provider.setFields([
        {
            fieldName: "Name",
            dataType: "text",
        },
        {
            fieldName: "Description",
            dataType: "text",
        }
    ]);

    // 컬럼 생성
    gridView.setColumns([
        {
            name: "NameColumn",
            fieldName: "Name",
            width: "200",
            header: {
                text: "Name",
            },
        },
        {
            name: "DescriptionColumn",
            fieldName: "Description",
            width: "1100",
            header: {
                text: "Description",
            },
        },
    ]);

    $.ajax({
        url: '/RealGrid/CategoryData',
        type: 'GET',
        success: function (response) {
            if (response.success) {
                provider.setRows(response.data);
            }
            else {
                console.error("Response error!");
            }
        },
        error: function (xhr, status, error) {
            console.error("ajax error!");
        }
    });

});