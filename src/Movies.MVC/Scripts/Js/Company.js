﻿function SearchCompany() {
    $('#search-company').on('click', function () {
        let searchValue = $('#search-value').val();
        window.location.href = '/company/list?fillter=' + searchValue;
    });
}

SearchCompany();

$('#btn-create').on('click', function (event) {

    var companyName = $('#companyName').val();
    var country = $('#country').val();
    var description = $('#description').val();

    var data = {
        Name: companyName,
        Country: country,
        Description: description
    };


    $.ajax({
        type: 'POST',
        url: '/Company/Create',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                Swal.fire({
                    title: "Success",
                    text: "Completed create film gener!",
                    icon: "success",
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Go to list"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/Company/List'
                    }
                });
            }
            else {
                let errorField = '#' + response.errorField + '-error';
                let message = response.message;
                $(errorField).text(message);
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
});

$('#btn-update').on('click', function (event) {

    var id = $('#companyId').text();
    var companyName = $('#companyName').val();
    var country = $('#country').val();
    var description = $('#description').val();

    var data = {
        Id: id,
        Country: country,
        Name: companyName,
        Description: description
    };


    $.ajax({
        type: 'POST',
        url: '/Company/Update',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            if (response.success) {
                Swal.fire({
                    title: "Success",
                    text: "Completed Update Company!",
                    icon: "success",
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Go to list"
                }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = '/Company/List'
                    }
                });
            }
            else {
                let errorField = '#' + response.errorField + '-error';
                let message = response.message;
                $(errorField).text(message);
            }
        },
        error: function (xhr, status, error) {
            console.error(error);
        }
    });
});

function Delete(id) {
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '/Company/Delete/?id=' + id,
                type: 'GET',
                success: function (response) {
                    if (response.success) {
                        Swal.fire({
                            title: "Success",
                            text: "Completed delete Company!",
                            icon: "success",
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Go to list"
                        }).then((result) => {
                            if (result.isConfirmed) {
                                window.location.href = '/Company/List'
                            }
                        });
                    }
                    else {
                        Swal.fire({
                            title: "Fail!",
                            text: response.message,
                            icon: "error"
                        });
                    }
                },
                error: function () {
                    console.error('Failed to delete data');
                }
            });
        }
    });
}

function RedirectUpdate(id) {
    window.location.href = '/Company/Update?id=' + id;
}

function RedirectCreate() {
    window.location.href = '/Company/Create';
}

function RemoveErrorMessage() {
    $('#companyName').on('click', function () {
        $('#companyName-error').text('');
    });

    $('#country').on('click', function () {
        $('#country-error').text('');
    });

    $('#description').on('click', function () {
        $('#description-error').text('');
    });
}

RemoveErrorMessage();
