﻿document.addEventListener('DOMContentLoaded', function () {
    // 설치
    const container = document.getElementById('realgrid');
    const provider = new RealGrid.LocalDataProvider(false);
    const gridView = new RealGrid.GridView(container);
    gridView.setDataSource(provider);

    // 필드 생성
    provider.setFields([
        {
            fieldName: "Title",
            dataType: "text",
        },
        {
            fieldName: "Release",
            dataType: "text",
        },
        {
            fieldName: "Hitting",
            dataType: "text",
        }
    ]);

    // 컬럼 생성
    gridView.setColumns([
        {
            name: "TitleColumn",
            fieldName: "Title",
            width: "500",
            header: {
                text: "Title",
            },
        },
        {
            name: "ReleaseColumn",
            fieldName: "Release",
            width: "400",
            header: {
                text: "Release",
            },
        },
        {
            name: "HittingColumn",
            fieldName: "Hitting",
            width: "300",
            header: {
                text: "Hitting",
            },
        }
    ]);

    $.ajax({
        url: '/RealGrid/MovieData',
        type: 'GET',
        success: function (response) {
            if (response.success) {
                provider.setRows(response.data);
            }
            else {
                console.error("Response error!");
            }
        },
        error: function (xhr, status, error) {
            console.error("ajax error!");
        }
    });

});