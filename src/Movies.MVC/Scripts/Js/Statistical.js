﻿function GetStatisticalMoviePie(callback) {
    $.ajax({
        url: '/Statistical/GetStatisticalMoviePie',
        type: 'GET',
        success: function (response) {
            if (response.success) {
                callback(null, response.data);
            } else {
                callback(new Error('Failed to get data'), null);
            }
        },
        error: function (xhr, status, error) {
            callback(new Error('AJAX error: ' + error), null);
        }
    });
}

function GetStatisticalMovieCol(callback) {
    $.ajax({
        url: '/Statistical/GetStatisticalMovieCol',
        type: 'GET',
        success: function (response) {
            if (response.success) {
                callback(null, response.data);
            } else {
                callback(new Error('Failed to get data'), null);
            }
        },
        error: function (xhr, status, error) {
            callback(new Error('AJAX error: ' + error), null);
        }
    });
}

