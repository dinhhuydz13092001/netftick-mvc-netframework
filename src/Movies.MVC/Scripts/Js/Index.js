﻿
function Movie_Fillter() {
    let mySelect = $('#slt_fillter');
    let sort = $("#is-sort").text();
    mySelect.change(function () {
        let url = "/?page=1";
        if (this.value != 'default') {
            url += "&fillter=" + this.value
        }
        if (sort != '') {
            url += "&sort=" + sort;
        }
        window.location.href = url;
    });
}

Movie_Fillter();

function Movie_Sort() {
    let mySelect = $('#slt_sort');
    let fillter = $('#is-fillter').text();
    mySelect.change(function () {
        let url = "/?page=1";
        if (this.value != 'default') {
            url += "&sort=" + this.value
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    });
}

Movie_Sort();

function SetSelectedFillterAndSort() {
    const sortValue = $("#is-sort").text();
    const fillterValue = $("#is-fillter").text();

    const sortSelect = $("#slt_sort")
    const fillterSelect = $("#slt_fillter")

    if (sortValue != '') {
        sortSelect.val(sortValue);
    }

    if (fillterValue != '') {
        fillterSelect.val(fillterValue);
    }
}

SetSelectedFillterAndSort();

function Paginated() {
    $('.btn-paginated').on('click', function () {
        var value = $(this).text();
        var fillter = $('#is-fillter').text();
        var sort = $('#is-sort').text();

        var url = '/?page=' + value;

        if (sort !== '') {
            url += '&sort=' + sort;
        }

        if (fillter !== '') {
            url += '&fillter=' + fillter;
        }

        window.location.href = url;
    });
}

Paginated();

function PreviousPage(currentPage) {
    if (currentPage > 1) {
        const fillter = $("#is-fillter").text();
        const sort = $("#is-sort").text();
        let url = "/?page=" + (currentPage - 1);
        if (sort != '') {
            url += "&sort=" + sort;
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    }
}

function NextPage(currentPage, totalPage) {
    if (currentPage < totalPage) {
        const fillter = $("#is-fillter").text();
        const sort = $("#is-sort").text();
        let url = "/?page=" + (currentPage + 1);
        if (sort != '') {
            url += "&sort=" + sort;
        }
        if (fillter != '') {
            url += "&fillter=" + fillter;
        }
        window.location.href = url;
    }
}

function ChangeColorPagenatedButton() {
    const pageIndex = $("#spn-currenPage").text();
    $('.btn-paginated').each(function () {
        if ($(this).text() == pageIndex) {
            $(this).removeClass('btn-outline-primary')
                .addClass('btn-primary');
        }
    });
}

ChangeColorPagenatedButton();

