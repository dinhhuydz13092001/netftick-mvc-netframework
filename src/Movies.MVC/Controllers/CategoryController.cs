﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System;
using Movies.MVC.Request;

namespace Movies.MVC.Controllers
{
    public class CategoryController : Controller
    {
        private IUnitOfWork _unitOfWork { get; set; }
        public CategoryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult List(string fillter)
        {
            Expression<Func<Category, bool>> fillterQuery = null;

            if (!string.IsNullOrEmpty(fillter))
            {
                //fillter here
                fillterQuery = f => (f.Name.Contains(fillter));
                ViewBag.fillter = fillter;
            }
            IEnumerable<Category> categories = _unitOfWork.Category.Get(fillterQuery);

            return View(categories);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                _unitOfWork.Category.Delete(id);
                _unitOfWork.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "General error, please try again!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CategoryRequest request)
        {
            if (string.IsNullOrEmpty(request.Name))
            {
                return Json(new { success = false, errorField = "categoryName", message = "Name can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Description))
            {
                return Json(new { success = false, errorField = "description", message = "Description can't be null!" });
            }

            Category category = new Category() { 
                Name = request.Name,
                Description = request.Description,
            };

            try
            {
                _unitOfWork.Category.Insert(category);
                _unitOfWork.Save();
                return Json(new { success = true, message = "Insert data sucessfully!." });
            }
            catch
            {
                return Json(new { success = false, errorField = "general", message = "General error, please try again!" });
            }
        }

        public ActionResult Update(int id)
        {
            Category category = _unitOfWork.Category.GetByID(id);
            return View(category);
        }

        [HttpPost]
        public ActionResult Update(Category request)
        {
            if (string.IsNullOrEmpty(request.Name))
            {
                return Json(new { success = false, errorField = "categoryName", message = "Name can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Description))
            {
                return Json(new { success = false, errorField = "description", message = "Description can't be null!" });
            }

            Category category = _unitOfWork.Category.GetByID(request.Id);
            category.Name = request.Name;
            category.Description = request.Description;

            try
            {
                _unitOfWork.Category.Update(category);
                _unitOfWork.Save();
                return Json(new { success = true, message = "Update data sucessfully!." });
            }
            catch
            {
                return Json(new { success = false, errorField = "general", message = "General error, please try again!" });
            }
        }
    }
}