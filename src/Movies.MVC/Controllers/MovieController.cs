﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;
using Movies.MVC.Helper;
using Movies.MVC.Request;
using Movies.MVC.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Movies.MVC.Controllers
{
    public class MovieController : Controller
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly int pageSize = 8;
        public MovieController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public ActionResult List(int? page, string fillter)
        {
            Expression<Func<Movie, bool>> fillterQuery = null;
            Func<IQueryable<Movie>, IOrderedQueryable<Movie>> sortQuery = null;

            if (!string.IsNullOrEmpty(fillter))
            {
                //fillter here
                fillterQuery = f => (f.Title.Contains(fillter));
                ViewBag.fillter = fillter;
            }

            //list category
            IEnumerable<Category> categories = _unitOfWork.Category.Get();
            ViewBag.Categories = categories;


            //list moive
            IEnumerable<Movie> movies = _unitOfWork.Movie.Get(fillterQuery, sortQuery, "");
            IEnumerable<MovieVM> movieVMs = movies.ToListVM();
            return View(PaginatedList<MovieVM>.Create(movieVMs, page ?? 1, pageSize));
        }

        // GET: Movie
        public ActionResult Detail(int id)
        {
            Movie movie = _unitOfWork.Movie.Get(x=>x.Id == id, null, "Category,PublishCompany").First();
            DetailMovieVM detailMovieVM = movie.ToDetailVM();
            return View(detailMovieVM);
        }

        public ActionResult Create()
        {
            var category = _unitOfWork.Category.Get();
            var publisCPN = _unitOfWork.PublishCompany.Get();

            ViewBag.Category = category;
            ViewBag.PublisCPN = publisCPN;
            return View();
        }

        [HttpPost]
        public ActionResult Create(MovieRequest request)
        {
            if (string.IsNullOrEmpty(request.FilmName))
            {
                return Json(new { success = false, errorField = "filmName", message = "Titile can't be null!" });
            }

            if (string.IsNullOrEmpty(request.PublishDay))
            {
                return Json(new { success = false, errorField = "publishDay", message = "PublishDay can't be null!" });
            }

            if (string.IsNullOrEmpty(request.ImageLink))
            {
                return Json(new { success = false, errorField = "imageLink", message = "ImageLink can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Description))
            {
                return Json(new { success = false, errorField = "description", message = "Description can't be null!" });
            }

            if (!int.TryParse(request.Hitting, out int hitting))
            {
                return Json(new { success = false, errorField = "hitting", message = "Hitting is not invalid!"});
            }

            if (!int.TryParse(request.Category, out int categoryId))
            {
                return Json(new { success = false, errorField = "category", message = "Category is not invalid!" });
            }

            if (!int.TryParse(request.Company, out int companyId))
            {
                return Json(new { success = false, errorField = "company", message = "Company is not invalid!" });
            }

            if (!DateTime.TryParse(request.PublishDay, out DateTime publishDay))
            {
                return Json(new { success = false, errorField = "publishDay", message = "PublishDay is not invalid!" });
            }

            Movie movie = new Movie() { 
                Title = request.FilmName,
                Hitting = hitting,
                CategoryId = categoryId,
                CreateAt = publishDay,
                Description = request.Description,
                PublishCompanyId = companyId,
                Image = request.ImageLink
            };

            try 
            {
                _unitOfWork.Movie.Insert(movie);
                _unitOfWork.Save();
                return Json(new { success = true, message = "Insert data sucessfully!." });
            }
            catch
            {
                return Json(new { success = false, errorField = "general", message = "General error, please try again!" });
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                _unitOfWork.Movie.Delete(id);
                _unitOfWork.Save();
                return Json(new { success = true}, JsonRequestBehavior.AllowGet);
            }
            catch {
                return Json(new { success = false, message = "General error, please try again!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Update(int id)
        {
            var category = _unitOfWork.Category.Get();
            var publisCPN = _unitOfWork.PublishCompany.Get();

            ViewBag.Category = category;
            ViewBag.PublisCPN = publisCPN;

            Movie movie = _unitOfWork.Movie.GetByID(id);

            UpdateMovieVM movieVM = movie.ToUpdateVM();

            return View(movieVM);
        }

        [HttpPost]
        public ActionResult Update(MovieRequest request, int id) {

            if (string.IsNullOrEmpty(request.FilmName))
            {
                return Json(new { success = false, errorField = "filmName", message = "Titile can't be null!" });
            }

            if (string.IsNullOrEmpty(request.PublishDay))
            {
                return Json(new { success = false, errorField = "publishDay", message = "PublishDay can't be null!" });
            }

            if (string.IsNullOrEmpty(request.ImageLink))
            {
                return Json(new { success = false, errorField = "imageLink", message = "ImageLink can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Description))
            {
                return Json(new { success = false, errorField = "description", message = "Description can't be null!" });
            }

            if (!int.TryParse(request.Hitting, out int hitting))
            {
                return Json(new { success = false, errorField = "hitting", message = "Hitting is not invalid!" });
            }

            if (!int.TryParse(request.Category, out int categoryId))
            {
                return Json(new { success = false, errorField = "category", message = "Category is not invalid!" });
            }

            if (!int.TryParse(request.Company, out int companyId))
            {
                return Json(new { success = false, errorField = "company", message = "Company is not invalid!" });
            }

            if (!DateTime.TryParse(request.PublishDay, out DateTime publishDay))
            {
                return Json(new { success = false, errorField = "publishDay", message = "PublishDay is not invalid!" });
            }

            Movie movie = _unitOfWork.Movie.GetByID(id);
            if (movie != null)
            {
                movie.Title = request.FilmName;
                movie.Hitting = hitting;
                movie.CategoryId = categoryId;
                movie.CreateAt = publishDay;
                movie.Description = request.Description;
                movie.PublishCompanyId = companyId;
                movie.Image = request.ImageLink;
            }

            try
            {
                _unitOfWork.Movie.Update(movie);
                _unitOfWork.Save();
                return Json(new { success = true, message = "Update data sucessfully!." });
            }
            catch
            {
                return Json(new { success = false, errorField = "general", message = "General error, please try again!" });
            }
        }
    }
}