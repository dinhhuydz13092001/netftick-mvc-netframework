﻿using Movies.DataAccess.Repository.IRepository;
using Movies.MVC.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Movies.MVC.Controllers
{
    public class StatisticalController : Controller
    {
        private IUnitOfWork _unitOfWork { get; set; }
        public StatisticalController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult MoviePieChart()
        {
            return View();
        }

        public ActionResult MovieColChart()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetStatisticalMovieCol() {
            var moives = _unitOfWork.Movie.Get();
            var categories = _unitOfWork.Category.Get();
            List<string> listCategory = new List<string>();
            List<int> countCate = new List<int>();
            foreach (var category  in categories)
            {
                listCategory.Add(category.Name);
                countCate.Add(moives.Where(x=>x.CategoryId == category.Id).Count());
            }
            return  Json(new 
                {
                    success = true,
                    data = new 
                    {
                        list = listCategory,
                        count = countCate
                    }
                },
                JsonRequestBehavior.AllowGet
            );
        }


        [HttpGet]
        public ActionResult GetStatisticalMoviePie()
        {
            var movies = _unitOfWork.Movie.Get();
            var categories = _unitOfWork.Category.Get();
            List<StatisticalMovieVM> statisticalMovieVM = new List<StatisticalMovieVM>();

            foreach (var category in categories)
            {
                statisticalMovieVM.Add(new StatisticalMovieVM
                {
                    name = category.Name,
                    y = movies.Where(x => x.CategoryId == category.Id).Count()
                });
            }
            return Json(new { success = true,  data = statisticalMovieVM}, JsonRequestBehavior.AllowGet);
        }
    }
}