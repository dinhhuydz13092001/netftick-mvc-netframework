﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;
using Movies.MVC.ViewModel;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Web.Mvc;
using System;
using Movies.MVC.Helper;

namespace Movies.MVC.Controllers
{
    public class HomeController : Controller
    {
        private IUnitOfWork _unitOfWork { get; set; }
        private readonly int pageSize = 8;
        public HomeController(IUnitOfWork unitOfWork) {
            _unitOfWork = unitOfWork;
        }
        
        public ActionResult Index(int? page, string fillter, string sort)
        {
            Expression<Func<Movie, bool>> fillterQuery = null;
            Func<IQueryable<Movie>, IOrderedQueryable<Movie>> sortQuery = null;

            if (!string.IsNullOrEmpty(fillter) && int.TryParse(fillter, out int cateId))
            {
                //fillter here
                fillterQuery = f => (f.Category.Id == cateId);
                ViewBag.fillter = fillter;
            }

            if (!string.IsNullOrEmpty(sort) && int.TryParse(sort, out int sortType))
            {
                switch (sortType)
                {
                    case 1:
                        sortQuery = o => o.OrderByDescending(x => x.Hitting);
                        break;

                    case 2:
                        sortQuery = o => o.OrderBy(x => x.CreateAt);
                        break;
                        
                    default:
                        break;
                }
                ViewBag.Sort = sort;
            }

            //list category
            IEnumerable<Category> categories = _unitOfWork.Category.Get();
            ViewBag.Categories = categories;


            //list moive
            IEnumerable<Movie> movies = _unitOfWork.Movie.Get(fillterQuery, sortQuery, "");
            IEnumerable<MovieVM> movieVMs = movies.ToListVM();
            return View(PaginatedList<MovieVM>.Create(movieVMs, page ?? 1, pageSize));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Error()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}