﻿using Movies.DataAccess.Repository.IRepository;
using Movies.MVC.Helper;
using System.Web.Mvc;

namespace Movies.MVC.Controllers
{
    public class RealGridController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public RealGridController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: RealGrid
        public ActionResult Movie()
        {
            return View();
        }

        public ActionResult Category()
        {
            return View();
        }

        public ActionResult StockingConfirmation()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CategoryData()
        {
            var categories = _unitOfWork.Category.Get();

            return Json(new { success = true, data = categories.ToListVM() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult MovieData()
        {
            var movies = _unitOfWork.Movie.Get();

            return Json(new { success = true, data = movies.ToListVM() }, JsonRequestBehavior.AllowGet);
        }
    }
}