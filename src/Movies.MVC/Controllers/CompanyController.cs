﻿using Movies.DataAccess.Repository.IRepository;
using Movies.DomainClass.Models;
using Movies.MVC.Request;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace Movies.MVC.Controllers
{
    public class CompanyController : Controller
    {
        private IUnitOfWork _unitOfWork { get; set; }
        public CompanyController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public ActionResult List(string fillter)
        {
            Expression<Func<PublishCompany, bool>> fillterQuery = null;

            if (!string.IsNullOrEmpty(fillter))
            {
                //fillter here
                fillterQuery = f => (f.Name.Contains(fillter));
                ViewBag.fillter = fillter;
            }
            IEnumerable<PublishCompany> companies = _unitOfWork.PublishCompany.Get(fillterQuery);

            return View(companies);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            try
            {
                _unitOfWork.PublishCompany.Delete(id);
                _unitOfWork.Save();
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new { success = false, message = "General error, please try again!" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(PublishCompanyRequest request)
        {
            if (string.IsNullOrEmpty(request.Name))
            {
                return Json(new { success = false, errorField = "companyName", message = "Name can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Country))
            {
                return Json(new { success = false, errorField = "country", message = "Country can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Description))
            {
                return Json(new { success = false, errorField = "description", message = "Description can't be null!" });
            }

            PublishCompany company = new PublishCompany()
            {
                Name = request.Name,
                Country = request.Country,
                Description = request.Description,
            };

            try
            {
                _unitOfWork.PublishCompany.Insert(company);
                _unitOfWork.Save();
                return Json(new { success = true, message = "Insert data sucessfully!." });
            }
            catch
            {
                return Json(new { success = false, errorField = "general", message = "General error, please try again!" });
            }
        }

        public ActionResult Update(int id)
        {
            PublishCompany company = _unitOfWork.PublishCompany.GetByID(id);
            return View(company);
        }

        [HttpPost]
        public ActionResult Update(PublishCompany request)
        {
            if (string.IsNullOrEmpty(request.Name))
            {
                return Json(new { success = false, errorField = "companyName", message = "Name can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Country))
            {
                return Json(new { success = false, errorField = "country", message = "Country can't be null!" });
            }

            if (string.IsNullOrEmpty(request.Description))
            {
                return Json(new { success = false, errorField = "description", message = "Description can't be null!" });
            }

            PublishCompany company = _unitOfWork.PublishCompany.GetByID(request.Id);
            company.Name = request.Name;
            company.Country = request.Country;
            company.Description = request.Description;

            try
            {
                _unitOfWork.PublishCompany.Update(company);
                _unitOfWork.Save();
                return Json(new { success = true, message = "Update data sucessfully!." });
            }
            catch
            {
                return Json(new { success = false, errorField = "general", message = "General error, please try again!" });
            }
        }
    }
}